# piTravelClock

A Raspberry Pi Zero based small, portable, travel clock using GPS for time and TZ sync.
![](docs/running-clock.jpg)

1. [The Hardware Side](#the-hardware-side)
2. [Installation](#installation)
3. [Systemd Startup on Boot](#systemd-startup-on-boot)

***
## The Hardware Side

### Parts List

 * [Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/)
 * [WaveShare 1.3" OLED display HAT](https://www.waveshare.com/product/modules/oleds-lcds/oled/1.3inch-oled-hat.htm)
 * [NMEA 0183 compatible GPS chip](https://www.aliexpress.com/item/Small-size-GNSS-GPS-GLONASS-module-GPS-receive-antenna-NEO-M8N-Solution-GNSS-module-Dual-GPS/32851353457.html?spm=a2g0s.9042311.0.0.ac3d4c4dyhKj7D)
 * [Vibrating Motor](https://www.aliexpress.com/item/5pcs-DC-Motor-3V-8mm-Coin-Flat-Vibrating-Micro-For-Pager-and-Cell-Phone-Mobile-Dcore/32880439935.html?spm=a2g0s.9042311.0.0.bd454c4dQxYRjV)
 * A Pi Zero Case
 * Soldering iron, solder, electrical tape 

### Assembly

Using the WaveShare OLED hat both simplifies and complicates the wiring.  The diagram below represents just the wiring for the GPS module and vibrating motor while ignoring the hat.  
  
![wiring](docs/wiring.png)

The way I was able to use the hat and solder additional components was install the GPIO header "upside down."  The female side of the hat was pretty short anyway, so running the long pins through the pi zero provided plenty of pin to solder the GPS and vibrating motor.

![](docs/hat-wiring.jpg)

## Installation

Starting with a fresh install of [Raspbian Stretch Lite](https://www.raspberrypi.org/downloads/raspbian/).  Make sure your system is up to date (`apt update && apt upgrade`).  Then install the packages needed to support NTP, GPS devices, and cloning the repo.

 * `# apt install ntp ntp-doc ntpstat gpsd gpsd-clients pps-tools git vim libjpeg9-dev libjpeg9`

### Clone the repo:

 * `$ git clone git@gitlab.com:shackledtodesk/piTravelClock.git`

### Setup python:

  * `# apt -y install python-setuptools python-dev build-essential`
Other libraries
  * `# apt install libjpeg-dev libghc-zlib-dev libtiff-dev libfreetype6-dev `
  * `# easy_install pip`
  * `# pip install -r requirements.txt`

### Manually Running:

  * `# python clock.py`

## Systemd Startup on Boot

From the `examples` directory there is a systemd service file.  Copy the file to `/etc/systemd/system/` then you can run:

  * `# systemctl daemon-reload`
  * `# systemctl enable start-clock`
  * `# systemctl start start-clock` or just reboot the machine.
