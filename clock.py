#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
piTravelClock: clock.py

Code for driving a RaspberryPi (Zero W) based clock.
Features:
 - Clock displays time/date and gps/wifi/alarm status.
 - GPS chip is used for time sync and determining location.
 - Timezone set based on lat/lon.
 - Ability to recheck location and reset TZ.
 - Turn display off/on.
 - Switch from local to UTC.
 - Turn wifi on/off (setting for SSID, etc require logging into Pi)
 - Basic alarm feature (set/enable/disable).
 - A Space Invaders game ripped from luma examples.
 - Reboot and shutdown from menus.

Usage: # ./clock.py or driven by systemd at bootup.
"""
from __future__ import unicode_literals

import os
import math
import time
import datetime
import textwrap

from faces import widgets
from faces import gps
from faces import location as fLoc
from faces import alarm
from faces import menu
from faces import wifi

from pytz import timezone
from luma.core.interface.serial import i2c, spi
from luma.core.render import canvas
from luma.core import lib
from timezonefinder import TimezoneFinder

from PIL import ImageFont

from luma.oled.device import sh1106
import RPi.GPIO as GPIO

cur_func = None
show_utc = None
disp_onoff = True
buzz = 0

#GPIO define
RST_PIN        = 25
CS_PIN         = 8
DC_PIN         = 24
KEY_UP_PIN     = 6
KEY_DOWN_PIN   = 19
KEY_LEFT_PIN   = 5
KEY_RIGHT_PIN  = 26
KEY_PRESS_PIN  = 13
KEY1_PIN       = 21
KEY2_PIN       = 20
KEY3_PIN       = 16

#init GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(KEY_UP_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)    # Input with pull-up
GPIO.setup(KEY_DOWN_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_LEFT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_RIGHT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY_PRESS_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY1_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)      # Input with pull-up
GPIO.setup(KEY2_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)      # Input with pull-up
GPIO.setup(KEY3_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)      # Input with pull-up
    
## 3 keys + joystick press
def press_key1(channel):
    global cur_func
    global show_utc
    global disp_onoff
    global buzz

    ## is the alarm going off?
    if buzz == 1:
        buzz = -1
        
    ## print cur_func
    if channel == KEY1_PIN:
        if cur_func == "location":
            cur_func = None
        else:
            cur_func = "location"
    elif channel == KEY2_PIN:
        if cur_func is None:
            if show_utc is None:
                show_utc = "UTC"
            else:
                show_utc = None
        elif cur_func == "location":
            cur_func = "reset_gps"            
    elif channel == KEY3_PIN:
        disp_onoff = not disp_onoff
    elif channel == KEY_PRESS_PIN:
        cur_func = "menu"
                
## Setup the GPIO keys
def init_gpio():
    GPIO.add_event_detect(KEY1_PIN, GPIO.FALLING, callback=press_key1, bouncetime=300)
    GPIO.add_event_detect(KEY2_PIN, GPIO.FALLING, callback=press_key1, bouncetime=300)
    GPIO.add_event_detect(KEY3_PIN, GPIO.FALLING, callback=press_key1, bouncetime=300)
    GPIO.add_event_detect(KEY_PRESS_PIN, GPIO.FALLING, callback=press_key1, bouncetime=300)

## we clear GPIO actions before going into a different menu
def clear_gpio():
    GPIO.remove_event_detect(KEY1_PIN)
    GPIO.remove_event_detect(KEY2_PIN)
    GPIO.remove_event_detect(KEY3_PIN)
    GPIO.remove_event_detect(KEY_PRESS_PIN)
        
#### the main clock face
def show_time(today_last_time=None, tz_name="UTC", gps_status=None, device=None):
    now = datetime.datetime.now(timezone(tz_name))
        
    today_date = now.strftime("%d %b %Y %Z")
    today_time = now.strftime("%l:%M:%S")
    ampm = now.strftime("%p")
    
    if today_time != today_last_time:
        with canvas(device) as draw:
            today_date = now.strftime("%d %b %Y %Z")
            
            cy = min(device.height, 64) / 2
            rx = device.width
            
            draw.text((0, 0), today_time, font=widgets.get_font("big"), fill="yellow")
            if (ampm == "PM"):
                draw.text(((rx - 20), 7), ampm,
                          font=widgets.get_font("small"), fill="white")
            else:
                draw.text(((rx - 20), 19), ampm,
                          font=widgets.get_font("small"), fill="white")

            draw.text((12, cy), today_date,
                      font=widgets.get_font("small"), fill="yellow")

            if gps.status(gps_status) == "locked":
                what = widgets.get_widget("globe")
            else:
                what = widgets.get_widget("sync")                
            draw.text((4, 44), text=what['itm'],
                      font=what['font'], fill="white")

            if wifi.status():
                what = widgets.get_widget("wifi")
                draw.text((20, 44), text=what['itm'],
                          font=what['font'], fill="white")
                
            ## alarm status
            if alarm.isSet():
                what = widgets.get_widget("alarm")
                draw.text((rx - 20, 44), text=what['itm'],
                          font=what['font'], fill="white")

                arm = alarm.read_conf()
                draw.text((rx - 50, 48),
                          text="{}:{:02}".format(arm["alarm"]["hour"], arm["alarm"]["minute"]),
                          font=widgets.get_font("small"), fill="white")
            else:
                what = widgets.get_widget("no_alarm")
                draw.text((rx - 20, 44), text=what['itm'],
                          font=what['font'], fill="white")

    return today_time

## clear the display
def wipe_display(device):
    device.clear()
            
##### startup the clock
def startup(device=None, max_try=5, first_time=True):
    if device is None:
        return None

    if first_time:
        init_msg="Starting up..."
    else:
        init_msg="Querying GPS..."
        os.system("systemctl restart ntp")
        
    with canvas(device) as draw:
        draw.text((0,0), text=init_msg, font=widgets.get_font("small"), fill="white")

    try_count = 0
    (dstream, agps) = gps.start()
    if agps is None:
        return None
    else:
        grec = gps.getRec(agps)
        if grec is not None:
            dstream.unpack(grec)
        else:
            dstream = None

        dots = "Looking for GPS sync "
        while gps.status(dstream) != "locked" and try_count <= max_try:
            grec = gps.getRec(agps)
            if grec is not None:
                print(gps.status(dstream))
                dstream.unpack(grec)

            dots += "."
            with canvas(device) as draw:
                draw.text((0,0), text=init_msg, font=widgets.get_font("small"), fill="white")
                offset = 12
                for line in textwrap.wrap(dots, width=21):
                    draw.text((0,offset), text=line, font=widgets.get_font("small"), fill="white")
                    offset += 12
                draw.text((100,50), text="{}".format(try_count), font=widgets.get_font("small"), fill="white")
                
            try_count += 1

    return(dstream)

## Get the local timezone based on GPS location
def get_tz(lon, lat):
    tf = TimezoneFinder()
    try:
        tz_name = tf.timezone_at(lng=lon,lat=lat)
        if tz_name is None:
            tz_name = "UTC"
    except ValueError:
        print ValueError
        print "{} {}".format(lon, lat)
        tz_name = "UTC"

    return(tz_name)

## The main thing is the main thing is the...
def main():
    global cur_func
    global show_utc
    global disp_onoff
    global buzz
    
    last_item = None

    ### Figure out where we are
    ### And set timezone accordingly
    gps_info = startup(device)
    location = gps.getLoc(gps_info)
    tz_name = get_tz(location['lon'], location['lat'])

    alarm.off()
    clear_gpio()
    init_gpio()
    ## Main Loop
    while True:
        # check alarm status on each loop
        if alarm.isSet():
            now = datetime.datetime.now(timezone(tz_name))
            nowish = now.strftime("%H%M")
            if alarm.check_time(nowish, buzz):
                if buzz == 0:
                    buzz = 1
                    disp_onoff = True
                    cur_func = None
            else:
                if buzz == -1:
                    buzz = 0

        if disp_onoff:
            if cur_func == "location":
                time.sleep(0.2)
                fLoc.show_location(location['lat'], location['lon'], tz_name, gps_info, device)
            elif cur_func == "reset_gps":
                cur_func = "location"
                gps_info = startup(device, 5, False)
                location = gps.getLoc(gps_info)
                tz_name = get_tz(location['lon'], location['lat'])
            elif cur_func == "menu":
                clear_gpio()
                time.sleep(0.2)
                menu.main(device)
                cur_func = None
                clear_gpio()
                init_gpio()
            else:
                if show_utc is None:
                    last_item = show_time(last_item, tz_name, gps_info, device)
                else:
                    last_item = show_time(last_item, "UTC", gps_info, device)
        else:
            wipe_display(device)

        if buzz == 1:
            alarm.buzz()
            
        time.sleep(0.1)

## run me, baby
if __name__ == "__main__":
    try:
        ## device = get_device()
        serial = spi(device=0, port=0, bus_speed_hz = 8000000, transfer_size = 4096, gpio_DC = DC_PIN, gpio_RST = RST_PIN)
        device = sh1106(serial, rotate=2)

        main()
    except KeyboardInterrupt:
        GPIO.cleanup()
        pass
