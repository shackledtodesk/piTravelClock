#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
alarm.py

Provides menu screen for setting alarm:
  - Set alarm time
  - Arm/disarm alarm
  - Check to see if it's time to fire alarm
  - Fire off the buzzer
"""
import os
import sys
import time
import json
import datetime

import widgets

from PIL import Image
from luma.core.render import canvas
import RPi.GPIO as GPIO

RST_PIN        = 25
CS_PIN         = 8
DC_PIN         = 24
KEY_UP_PIN     = 6
KEY_DOWN_PIN   = 19
KEY_LEFT_PIN   = 5
KEY_RIGHT_PIN  = 26
KEY_PRESS_PIN  = 13
KEY3_PIN       = 16

BUZZER         = 12

joy_action = 0
is_set = False

alarm_file = "/home/andy/piTravelClock/alarm-ex.json"

GPIO.setmode(GPIO.BCM)
GPIO.setup(KEY_UP_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)    # Input with pull-up
GPIO.setup(KEY_DOWN_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_LEFT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_RIGHT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY_PRESS_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY3_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up

## buzzer
GPIO.setup(BUZZER, GPIO.OUT)


def joystick(channel):
    global joy_action
    joy_action = 0
    if channel == KEY_UP_PIN:
        joy_action = 1
    elif channel == KEY_DOWN_PIN:
        joy_action = 2
    elif channel == KEY_LEFT_PIN:
        joy_action = 3
    elif channel == KEY_RIGHT_PIN:
        joy_action = 4
    if channel == KEY_PRESS_PIN:
        joy_action = 5
    if channel == KEY3_PIN:
        joy_action = 99
        
def isSet(settings=None):
    if settings is None:
        settings = read_conf()
    if settings is None:
        return False
    else:
        return settings["alarm"]["active"]

def doSet(settings=None):
    if settings is None:
        settings = read_conf()

    if settings["alarm"]["active"]:
        settings["alarm"]["active"] = False
    else:
        settings["alarm"]["active"] = True

    write_conf(settings)

def buzz():
    GPIO.output(BUZZER, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(BUZZER, GPIO.LOW)

def off():
    GPIO.output(BUZZER, GPIO.LOW)
    
def read_conf():
    global alarm_file

    if os.path.isfile(alarm_file):
        with open(alarm_file, 'r') as f:
            try:
                alarm_settings = json.load(f)
            except:
                alarm_settings["alarm"]["active"] = False
    else:
        return None

    return alarm_settings


def write_conf(config=None):
    global alarm_file
    
    with open(alarm_file, 'w') as f:
        json.dump(config, f, indent=4)

    
def check_time(nowish=None, buzz=False):
    if nowish is None:
        return False

    ## get the alarm time
    settings = read_conf()
    if "hour" in settings["alarm"] and "minute" in settings["alarm"]:
        al_tm = "{}{}".format(settings["alarm"]["hour"],settings["alarm"]["minute"])
    else:
        return False
    
    ## check alarm time versus nowish
    d_time = int(nowish) - int(al_tm)
    if int(nowish) == int(al_tm):
        return True
    else:
        return False

    ## should never get here, really
    return False
    
def clean_up():
    global joy_action
    joy_action = 0
    GPIO.remove_event_detect(KEY_UP_PIN)
    GPIO.remove_event_detect(KEY_DOWN_PIN)
    GPIO.remove_event_detect(KEY_LEFT_PIN)
    GPIO.remove_event_detect(KEY_RIGHT_PIN)
    GPIO.remove_event_detect(KEY_PRESS_PIN)
    GPIO.remove_event_detect(KEY3_PIN)
    return

def init_gpio():
    global joy_action
    joy_action = 0
    GPIO.add_event_detect(KEY_UP_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_DOWN_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_LEFT_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_RIGHT_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_PRESS_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY3_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    return


def m_time(hrmn, hr, mn, joy_action):
    if joy_action == 1:
        if hrmn == 1:
            mn += 1
            if mn > 59:
                mn = 0
        else:
            hr += 1
            if hr > 23:
                hr = 0
    elif joy_action == 2:
        if hrmn == 1:
            mn -= 1
            if mn < 0:
                mn = 59
        else:
            hr -= 1
            if hr < 0:
                hr = 23
    elif joy_action == 3 or joy_action == 4:
        if hrmn == 1:
            hrmn = 0
        else:
            hrmn = 1
        joy_action = 0
    elif joy_action == 5:
        joy_action = 0
        hrmn = 3
    elif joy_action == 99:
        hrmn = -1

    return(hrmn, hr, mn)
        
def m_menu(actions, joy_action, itm, set_time, settings):
    if joy_action == 1:
        if itm > 0:
            itm -= 1
        else:
            itm = 0

    elif joy_action == 2:
        if itm < (len(actions) - 1):
            itm += 1
        else:
            itm = len(actions) - 1

    elif joy_action == 3 or joy_action == 99:
        joy_action = 0
        clean_up()
        return(None, None)
    elif joy_action == 4 or joy_action == 5:
        if itm == 0:
            doSet(settings)
        else:
            set_time = not set_time

    return(itm, set_time)

def main(device):
    global joy_action
    joy_action = 0
    mn_itm = 0
    init_gpio()

    actions = [ 'Alarm On/Off', 'Set Alarm' ]
    settings = read_conf()

    hrmn = 0
    
    if "hour" in settings["alarm"] and "minute" in settings["alarm"]:
        al_hr = settings["alarm"]["hour"]
        al_mn = settings["alarm"]["minute"]
    else:
        al_hr = 0
        al_mn = 0
        
    set_time = False
    
    while True:
        if set_time:
            (hrmn, al_hr, al_mn) = m_time(hrmn, al_hr, al_mn, joy_action)
            joy_action = 0
            time.sleep(0.1)
            if hrmn == 3:
                hrmn = 0
                settings["alarm"]["hour"] = al_hr
                settings["alarm"]["minute"] = al_mn
                write_conf(settings)
                set_time = False
            elif hrmn == -1:
                hrmn = 0
                set_time = False
                if "hour" in settings["alarm"] and "minute" in settings["alarm"]:
                    al_hr = settings["alarm"]["hour"]
                    al_mn = settings["alarm"]["minute"]
                else:
                    al_hr = 0
                    al_mn = 0
        else:
            (mn_itm, set_time) = m_menu(actions, joy_action, mn_itm, set_time, settings)
            joy_action = 0
            if mn_itm is None:
                return
        
        with canvas(device) as draw:
            for i in range(len(actions)):
                draw.text((12, (i * 14)), actions[i],
                          font=widgets.get_font("small"), fill="white")

            if set_time:
                arrow = widgets.get_widget("up_a")
                draw.text((46 + (hrmn * 32), 24), arrow['itm'],
                          font=arrow['font'], fill="white")
                arrow = widgets.get_widget("dn_a")
                draw.text((46 + (hrmn * 32), 49), arrow['itm'],
                          font=arrow['font'], fill="white")
            else:
                arrow = widgets.get_widget("gt")
                draw.text((0, (mn_itm * 13)), arrow['itm'],
                          font=arrow['font'], fill="white")

            if isSet(settings):
                what = widgets.get_widget("alarm")
                draw.text((90, 0), text=what['itm'],
                          font=what['font'], fill="white")
            else:
                what = widgets.get_widget("no_alarm")
                draw.text((90, 0), text=what['itm'],
                          font=what['font'], fill="white")
                
            draw.text((40, 32), text="{:02}:{:02}".format(al_hr, al_mn),
                      font=widgets.get_font("med"), fill="white")


if __name__ == "__main__":
    from luma.core.interface.serial import i2c, spi
    from luma.oled.device import sh1106
    
    try:
        serial = spi(device=0, port=0, bus_speed_hz = 8000000, transfer_size = 4096, gpio_DC = DC_PIN, gpio_RST = RST_PIN)
        device = sh1106(serial, rotate=2)

        main(device)
    except KeyboardInterrupt:
        GPIO.cleanup()
        pass

