#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Might need to do this to init the gpsd channel
# echo '?WATCH={"json":true,"pps":true,"enable":true};' | nc localhost 2947
# 
# info: http://www.catb.org/gpsd/gpsd_json.html

"""
gps.py

Basically a wrapper around gps3 code to determine gps sync
and lat/lon.  Relies upon gpsd.
"""

import time
from gps3 import gps3

## How many satellites does our chip have a lock on.
def satellites_used(feed):
    """Counts number of satellites used in calculation from total visible satellites
    Arguments:  feed feed=data_stream.satellites
    Returns:
    total_satellites(int):
    used_satellites (int):
    """
    total_satellites = 0
    used_satellites = 0
    
    if not isinstance(feed, list):
        return 0, 0
    
    for satellites in feed:
        total_satellites += 1
        if satellites['used'] is True:
            used_satellites += 1
    return total_satellites, used_satellites

## GPS signal sync status        
def status(sats=None):
    if sats is None:
        return "initializing"

    if sats.TPV is None:
        return "initializing"
    
    if (sats.TPV['mode'] != "n/a") and (sats.TPV['mode'] > 1):
        return "locked"
    else:
        return "seeking"

## Retrieve lat/lon or provide a default in Pacific Timezone
def getLoc(data=None):
    if data is None: 
        lat = 37.5064
        lon = -122.2901
    elif data.TPV is None or data.TPV['lat'] == 'n/a':
        lat = 37.5064
        lon = -122.2901
    else:
        lat = data.TPV['lat']
        lon = data.TPV['lon']
    
    return {"lat": lat, "lon": lon}

## initialize the GPS watcher
def start():
    try:
        agps = gps3.GPSDSocket()
        data_stream = gps3.DataStream()
        agps.connect()
        agps.watch()
                
        agps.send('?WATCH={"enable":true,"json":true};')
        agps.send("?POLL;")
    except:
        data_stream = { "TPV" : None, "SKY": None }
        return(data_stream, None)

    if data_stream is None:
        data_stream = { "TPV" : None, "SKY": None }
    return (data_stream, agps)

## Get a NMEA record from gpsd stream
def getRec(gsock=None):
    if gsock is None:
        return None

    tries = 0
    for ndata in gsock:
        tries += 1
        if ndata:
            return ndata
        else:
            tries += 1
            if tries > 10000:
                #print "bailing on gps"
                return None

## Example run if done from commandline.
if __name__ == "__main__":
    (dstream, agps) = start()

    grec = getRec(agps)
    if grec is None:
        dstream = None
        agps = None
    else:
        print "what"
        print grec
        dstream.unpack(grec)

    thing = status(dstream)
    
    while status(dstream) != "locked":
        grec = getRec(agps)
        if grec is None:
            dstream = None
            agps = None
            break
        else:
            dstream.unpack(grec)

    print status(dstream)
    print "Lat: {lat} / Lon: {lon}".format(**getLoc(dstream))
            
    if status(dstream) != "initializing":
        print "Fix Status: {} {}".format(dstream.TPV['mode'], status(dstream))
        print "Satellites: {0[1]}/{0[0]}".format(satellites_used(dstream.SKY['satellites']))

