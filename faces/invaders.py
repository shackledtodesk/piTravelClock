#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2014-18 Richard Hull and contributors
# See LICENSE.rst for details.
# Mangled by Andy Mayhew for GPIO joystick control


"""
Space Invaders demo.

Ported from:
https://gist.github.com/TheRayTracer/dd12c498e3ecb9b8b47f#file-invaders-py
"""

import os.path
import time
import random

from PIL import Image
from luma.core.render import canvas
from luma.core.sprite_system import framerate_regulator
import RPi.GPIO as GPIO

RST_PIN        = 25
CS_PIN         = 8
DC_PIN         = 24
KEY_UP_PIN     = 6
KEY_DOWN_PIN   = 19
KEY_LEFT_PIN   = 5
KEY_RIGHT_PIN  = 26
KEY_PRESS_PIN  = 13
KEY3_PIN       = 16

joy_action = 0

GPIO.setmode(GPIO.BCM)
# GPIO.setup(KEY_UP_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)    # Input with pull-up
# GPIO.setup(KEY_DOWN_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_LEFT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_RIGHT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY_PRESS_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY3_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
    
def joystick(channel):
    global joy_action
    joy_action = 0
    if channel == KEY_LEFT_PIN:
        joy_action = 1
    if channel == KEY_RIGHT_PIN:
        joy_action = 2
    if channel == KEY_PRESS_PIN:
        joy_action = 3
    if channel == KEY3_PIN:
        joy_action = 99
        
arrow = [0x04, 0x02, 0x01, 0x02, 0x04]
alien1 = [0x4C, 0x1A, 0xB6, 0x5F, 0x5F, 0xB6, 0x1A, 0x4C]
alien2 = [0x18, 0xFD, 0xA6, 0x3C, 0x3C, 0xA6, 0xFD, 0x18]
alien3 = [0xFC, 0x98, 0x35, 0x7E, 0x7E, 0x35, 0x98, 0xFC]
ARMY_SIZE_ROWS = 2
ARMY_SIZE_COLS = 6

class bullet(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.alive = False

    def render(self, draw):
        if self.alive:
            draw.line((self.x, self.y, self.x, self.y + 2), fill="white")

    def reset(self, x, y):
        self.x = x
        self.y = y
        self.alive = True
        return

    def update(self, direction):
        if self.alive:
            self.y = self.y + (direction * 4)
            if self.y < 10:
                self.alive = False


class player(object):
    def __init__(self):
        self.x = 48
        self.y = 54
        self.bullets = [bullet(0, 0) for _ in range(4)]

    def render(self, draw):
        for i in range(len(arrow)):
            line = arrow[i]
            for j in range(3):
                if line & 0x1:
                    draw.point((self.x - 2 + i, self.y + j), fill="white")
                line >>= 1

        for bullet in self.bullets:
            bullet.render(draw)

    def update(self, direction):
        t = self.x + (direction * 2)
        if t > 4 and t < 92:
            self.x = t
        for bullet in self.bullets:
            bullet.update(-1)

    def shoot(self):
        for bullet in self.bullets:
            if not bullet.alive:
                bullet.reset(self.x, self.y)
                break


class invader(object):
    def __init__(self, minx, maxx, x, y):
        self.x = x
        self.y = y
        self._direction = 1
        self.alive = True
        self.score = 10
        self._minx = minx
        self._maxx = maxx
        return

    def render(self, draw):
        if self.alive:
            for i in range(len(alien2)):
                line = alien2[i]
                for j in range(8):
                    if line & 0x1:
                        draw.point((self.x - 4 + i, self.y - 4 + j), "green")
                    line >>= 1

    def update(self):
        invaded = False
        if self.alive:
            t = self.x + self._direction
            if t > self._minx and t < self._maxx:
                self.x = self.x + self._direction
            else:
                self._direction = self._direction * -1
                self.y = self.y + 2
                if self.y > 44:
                    invaded = True
        return invaded


class army(object):
    def __init__(self):
        self.invaded = False
        self.invaders = []
        for i in range(ARMY_SIZE_ROWS):
            for j in range(ARMY_SIZE_COLS):
                minx = 4 + (j * 12)
                maxx = 30 + (j * 12)
                x = 4 + (j * 12)
                y = 14 + (i * 12)
                self.invaders.append(invader(minx, maxx, x, y))

    def render(self, draw):
        for invader in self.invaders:
            invader.render(draw)

    def update(self, bullets):
        for invader in self.invaders:
            if invader.update():
                self.invaded = True

        for invader in self.invaders:
            if invader.alive:
                for bullet in bullets:
                    if bullet.alive:
                        t = (invader.x - bullet.x) * (invader.x - bullet.x) + (invader.y - bullet.y) * (invader.y - bullet.y)
                        # if point is in circle
                        if t < 25:  # 5 * 5 = r * r
                            invader.alive = False
                            bullet.alive = False

    def size(self):
        size = 0
        for invader in self.invaders:
            if invader.alive:
                size += 1
        return size

    def score(self):
        score = 0
        for invader in self.invaders:
            if not invader.alive:
                score += invader.score
        return score


def ai_logic_shoot(army, plyr):
    for invader in army.invaders:
        if invader.alive:
            if plyr.x > (invader.x - 2) and plyr.x < (invader.x + 2):
                if random.random() < 0.75:
                    plyr.shoot()
                    return


def ai_logic_move(army, plyr, rows):
    for i in rows:
        invader = army.invaders[i]
        if invader.alive:
            if plyr.x < invader.x:
                plyr.update(1)
                return
            elif plyr.x > invader.x:
                plyr.update(-1)
                return
        i += 1

def clean_up():
    GPIO.remove_event_detect(KEY_LEFT_PIN)
    GPIO.remove_event_detect(KEY_RIGHT_PIN)
    GPIO.remove_event_detect(KEY_PRESS_PIN)
    GPIO.remove_event_detect(KEY3_PIN)
    return

def game(device):
    global joy_action

    GPIO.add_event_detect(KEY_LEFT_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_RIGHT_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_PRESS_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY3_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)

    regulator = framerate_regulator()
    plyr = player()
    aliens = army()
    rows = random.sample(range(12), 12)
    for i in range(3):
        with canvas(device) as draw:
            draw.text((26, 28), text="Space Invaders", fill="blue")

            draw.text((26, 45), text="Starting in {}".format(3 - i), fill="white")
            time.sleep(1)
    device.clear()
    
    while not aliens.invaded and aliens.size() > 0:
        with regulator:
            with canvas(device) as draw:
                draw.line((0, 61, 95, 61), fill="white")
                draw.line((0, 63, 95, 63), fill="white")
                
                if joy_action == 1:
                    plyr.update(-1)
                elif joy_action == 2:
                    plyr.update(1)
                if joy_action == 3:
                    joy_action = 4
                    plyr.shoot()
                    plyr.update(0)
                else:
                    plyr.update(0)

                if joy_action == 99:
                    clean_up()
                    return
                aliens.update(plyr.bullets)
                
                aliens.render(draw)
                plyr.render(draw)
                
                draw.text((8, 0), text="Score: {0}".format(aliens.score()), fill="blue")
                
    # Double buffering in pygame?
    for i in range(2):
        with canvas(device) as draw:
            if aliens.size() == 0:
                draw.text((27, 28), text="Victory", fill="blue")
            else:
                draw.text((30, 28), text="Defeat", fill="red")
                    
    time.sleep(3)
    clean_up()
    return

if __name__ == '__main__':
    from luma.core.interface.serial import i2c, spi
    from luma.oled.device import sh1106
    
    try:
        serial = spi(device=0, port=0, bus_speed_hz = 8000000, transfer_size = 4096, gpio_DC = DC_PIN, gpio_RST = RST_PIN)
        device = sh1106(serial, rotate=2)
    except KeyboardInterrupt:
        GPIO.cleanup()
        pass
    
    if device.width < 96 or device.height < 64:
        raise ValueError("Unsupported mode: {0}x{1}".format(device.width, device.height))

    try:
        game(device)
    except KeyboardInterrupt:
        GPIO.cleanup()
        pass
