#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
location.py

A screen for the clock showing location (lat/lon), 
GPS status, and timezone name.

TODO: Add ability to set/force timezone.
"""

from PIL import ImageFont
from luma.core.interface.serial import i2c, spi
from luma.core.render import canvas
from luma.core import lib
import widgets
import gps
import datetime

#################
def show_location(lat, lon, tz_name, gps_info, device):
    with canvas(device) as draw:
        lat = "Lat: {}".format(lat)
        lon = "Lon: {}".format(lon)
        tzone = "{}".format(tz_name)
        gps_status = gps.status(gps_info)
        
        rx = device.width
        ry = device.height
        draw.text((0, 0), lat,
                  font=widgets.get_font("small"), fill="white")
        draw.text((0, 12), lon,
                  font=widgets.get_font("small"), fill="white")
        draw.text((0, 48), text="GPS Status: {}".format(gps_status),
                  font=widgets.get_font("small"), fill="white")
        draw.text((0, 24), "Time Zone:",
                  font=widgets.get_font("small"), fill="white")
        draw.text((0, 36), tzone,
                  font=widgets.get_font("small"), fill="white")

        sync = widgets.get_widget("sync")
        draw.text((rx - 14, 20), sync["itm"],
                  font=sync["font"], fill="white")

