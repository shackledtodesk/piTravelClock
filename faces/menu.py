#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
menu.py

Menu system for clock. 

"""

import os.path
import time

from PIL import Image
from luma.core.render import canvas
import RPi.GPIO as GPIO
import os
import sys
import psutil
import netifaces
import datetime

import widgets
import invaders
import alarm
import settings

RST_PIN        = 25
CS_PIN         = 8
DC_PIN         = 24
KEY_UP_PIN     = 6
KEY_DOWN_PIN   = 19
KEY_LEFT_PIN   = 5
KEY_RIGHT_PIN  = 26
KEY_PRESS_PIN  = 13
KEY3_PIN       = 16

joy_action = 0

GPIO.setmode(GPIO.BCM)
GPIO.setup(KEY_UP_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)    # Input with pull-up
GPIO.setup(KEY_DOWN_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_LEFT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_RIGHT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY_PRESS_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY3_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
    
def joystick(channel):
    global joy_action
    joy_action = 0
    if channel == KEY_UP_PIN:
        joy_action = 1
    elif channel == KEY_DOWN_PIN:
        joy_action = 2
    elif channel == KEY_LEFT_PIN:
        joy_action = 3
    elif channel == KEY_RIGHT_PIN:
        joy_action = 4
    if channel == KEY_PRESS_PIN:
        joy_action = 5
    if channel == KEY3_PIN:
        joy_action = 99

def clean_up():
    GPIO.remove_event_detect(KEY_UP_PIN)
    GPIO.remove_event_detect(KEY_DOWN_PIN)
    GPIO.remove_event_detect(KEY_LEFT_PIN)
    GPIO.remove_event_detect(KEY_RIGHT_PIN)
    GPIO.remove_event_detect(KEY_PRESS_PIN)
    GPIO.remove_event_detect(KEY3_PIN)
    return

def init_gpio():
    GPIO.add_event_detect(KEY_UP_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_DOWN_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_LEFT_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_RIGHT_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_PRESS_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY3_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    return

def bytes2human(n):
    """
    >>> bytes2human(10000)
    '9K'
    >>> bytes2human(100001221)
    '95M'
    """
    symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
    prefix = {}
    for i, s in enumerate(symbols):
        prefix[s] = 1 << (i + 1) * 10
    for s in reversed(symbols):
        if n >= prefix[s]:
            value = int(float(n) / prefix[s])
            return '{}{}'.format(value, s)
    return "{}B".format(n)

def show_system(device):
    ## get network interface
    try:
        gws = netifaces.gateways()
        what = gws['default'][netifaces.AF_INET][1]
        iface = netifaces.ifaddresses(what)
        addr = iface[netifaces.AF_INET]
        ip = addr[0]['addr']
        nwork = "{}: {}".format(what, ip)
    except:
        nwork = "no functional network"

    uptime = datetime.datetime.now() - datetime.datetime.fromtimestamp(psutil.boot_time())
    av1, av2, av3 = os.getloadavg()
    
    usage = psutil.virtual_memory()
    mem_usage = "Mem: {} {:.0f}%".format(bytes2human(usage.used), 100 - usage.percent)

    usage = psutil.disk_usage('/')
    disk_usage = "SD: {} {:.0f}%".format(bytes2human(usage.used), usage.percent)
    
    rx = device.width
    ry = device.height

    with canvas(device) as draw:
        draw.text((0, 0), "Up: {}".format(str(uptime).split('.')[0]),
                  font=widgets.get_font("small"), fill="white")
        draw.text((0, 12), "Ld: {}, {}, {}".format(av1, av2, av3),
                  font=widgets.get_font("small"), fill="white")
        draw.text((0, 24), mem_usage,
                  font=widgets.get_font("small"), fill="white")
        draw.text((0, 36), disk_usage,
                  font=widgets.get_font("small"), fill="white")                  
        draw.text((0, 48), nwork,
                  font=widgets.get_font("small"), fill="white")

def shutdown(device):
    global joy_action
    joy_action = 0
    mn_itm = 2

    while True:
        if joy_action == 1:
            if mn_itm == 1:
                mn_itm = 3
            else:
                mn_itm -= 1
            joy_action = 0
        elif joy_action == 2:
            if mn_itm == 3:
                mn_itm = 1
            else:
                mn_itm += 1
            joy_action = 0
                
        elif joy_action == 3 or joy_action == 99:
            joy_action = 0
            return
        elif joy_action == 4 or joy_action == 5:
            if mn_itm == 1:
                with canvas(device) as draw:
                    draw.text((12, 2), "Shutting down now...",
                              font=widgets.get_font("small"), fill="white")
                    draw.text((12, 17), "Please wait.",
                              font=widgets.get_font("small"), fill="white")
                    time.sleep(2)
                    ## requires root privs
                    os.system("sudo /sbin/poweroff")
            elif mn_itm == 3:
                with canvas(device) as draw:
                    draw.text((12, 2), "Rebooting now...",
                              font=widgets.get_font("small"), fill="white")
                    draw.text((12, 17), "Please wait.",
                              font=widgets.get_font("small"), fill="white")
                    time.sleep(2)
                    ## requires root privs
                    os.system("sudo /sbin/reboot")
            else:
                joy_action = 0
                return
        
        with canvas(device) as draw:
            draw.text((12, 2), "Shutdown ",
                      font=widgets.get_font("small"), fill="white")
            draw.text((12, 17), "Yes ",
                      font=widgets.get_font("small"), fill="white")
            draw.text((12, 32), "No ",
                      font=widgets.get_font("small"), fill="white")
            draw.text((12, 47), "Reboot",
                      font=widgets.get_font("small"), fill="white")
            arrow = widgets.get_widget("gt")
            draw.text((0, (mn_itm * 15)), arrow['itm'],
                      font=arrow['font'], fill="white")

        time.sleep(0.1)


def main(device):
    global joy_action
    joy_action = 0
    mn_itm = 0
    init_gpio()

    time.sleep(0.1)
    while True:
        if joy_action == 1:
            if mn_itm > 0:
                mn_itm -= 1
            else:
                mn_itm = 0
            joy_action = 0

        elif joy_action == 2:
            if mn_itm < 4:
                mn_itm += 1
            else:
                mn_itm = 4
            joy_action = 0
        elif joy_action == 3 or joy_action == 99:
            joy_action = 0
            clean_up()
            return
        elif joy_action == 4 or joy_action == 5:
            joy_action = 0
            if mn_itm == 0:                
                with canvas(device) as draw:
                    draw.text((12, 0), "Loading...",
                              font=widgets.get_font("small"), fill="white")
                print "space invaders"
                clean_up()
                invaders.game(device)
                init_gpio()
            elif mn_itm == 1:
                print "alarm"
                clean_up()
                joy_action = 0
                time.sleep(0.2)
                alarm.main(device)
                init_gpio()
            elif mn_itm == 2:
                print "settings"
                clean_up()
                joy_action = 0
                time.sleep(0.2)
                settings.main(device)
                init_gpio()
            elif mn_itm == 3:
                while joy_action == 0:
                    show_system(device)
                    time.sleep(0.1)
            elif mn_itm == 4:
                shutdown(device)

        with canvas(device) as draw:
            draw.text((12, 0), "Space Invaders",
                      font=widgets.get_font("small"), fill="white")
            draw.text((12, 14), "Alarm ",
                      font=widgets.get_font("small"), fill="white")
            draw.text((12, 28), "Settings ",
                      font=widgets.get_font("small"), fill="white")
            draw.text((12, 42), "System Info",
                      font=widgets.get_font("small"), fill="white")
            draw.text((12, 56), "Shutdown ",
                      font=widgets.get_font("small"), fill="white")
            arrow = widgets.get_widget("gt")
            draw.text((0, (mn_itm * 13)), arrow['itm'],
                      font=arrow['font'], fill="white")

        

if __name__ == "__main__":
    from luma.core.interface.serial import i2c, spi
    from luma.oled.device import sh1106
    
    try:
        serial = spi(device=0, port=0, bus_speed_hz = 8000000, transfer_size = 4096, gpio_DC = DC_PIN, gpio_RST = RST_PIN)
        device = sh1106(serial, rotate=2)
        main(device)
    except KeyboardInterrupt:
        GPIO.cleanup()
        pass

