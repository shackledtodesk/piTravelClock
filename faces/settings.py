#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
settings.py

Skeleton for a clock settings menu.
Intended features:
  - Set time/date
  - Set timezone
  - Wifi on/off (implemented)
"""

import os.path
import time

from PIL import Image
from luma.core.render import canvas
import RPi.GPIO as GPIO
import widgets
import alarm
import wifi
import os
import sys
import datetime

RST_PIN        = 25
CS_PIN         = 8
DC_PIN         = 24
KEY_UP_PIN     = 6
KEY_DOWN_PIN   = 19
KEY_LEFT_PIN   = 5
KEY_RIGHT_PIN  = 26
KEY_PRESS_PIN  = 13
KEY1_PIN       = 21
KEY2_PIN       = 20
KEY3_PIN       = 16

joy_action = 0

GPIO.setmode(GPIO.BCM)
GPIO.setup(KEY_UP_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)    # Input with pull-up
GPIO.setup(KEY_DOWN_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_LEFT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # Input with pull-up
GPIO.setup(KEY_RIGHT_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY_PRESS_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
GPIO.setup(KEY3_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Input with pull-up
    
def joystick(channel):
    global joy_action
    joy_action = 0
    if channel == KEY_UP_PIN:
        joy_action = 1
    elif channel == KEY_DOWN_PIN:
        joy_action = 2
    elif channel == KEY_LEFT_PIN:
        joy_action = 3
    elif channel == KEY_RIGHT_PIN:
        joy_action = 4
    if channel == KEY_PRESS_PIN:
        joy_action = 5
    if channel == KEY3_PIN:
        joy_action = 99

def clean_up():
    GPIO.remove_event_detect(KEY_UP_PIN)
    GPIO.remove_event_detect(KEY_DOWN_PIN)
    GPIO.remove_event_detect(KEY_LEFT_PIN)
    GPIO.remove_event_detect(KEY_RIGHT_PIN)
    GPIO.remove_event_detect(KEY_PRESS_PIN)
    GPIO.remove_event_detect(KEY3_PIN)
    return

def init_gpio():
    GPIO.add_event_detect(KEY_UP_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_DOWN_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_LEFT_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_RIGHT_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY_PRESS_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    GPIO.add_event_detect(KEY3_PIN, GPIO.FALLING, callback=joystick, bouncetime=300)
    return


def main(device):
    global joy_action
    joy_action = 0
    mn_itm = 0
    init_gpio()

    actions = [ 'Set Timezone', 'Set Time', 'Set Date', 'Wifi On/Off' ]
    
    while True:
        if joy_action == 1:
            if mn_itm > 0:
                mn_itm -= 1
            else:
                mn_itm = 0
            joy_action = 0

        elif joy_action == 2:
            if mn_itm < (len(actions) - 1):
                mn_itm += 1
            else:
                mn_itm = len(actions) - 1
            joy_action = 0
        elif joy_action == 3 or joy_action == 99:
            joy_action = 0
            clean_up()
            return
        elif joy_action == 4 or joy_action == 5:
            if actions[mn_itm] == actions[0]:
                print 'tz'
            elif actions[mn_itm] == actions[1]:
                print 'time'
            elif actions[mn_itm] == actions[2]:
                print 'date'
            elif actions[mn_itm] == actions[3]:
                if wifi.status():
                    wifi.turnoff()
                else:
                    wifi.turnon()
                time.sleep(0.2)
                print 'wifi'

            joy_action = 0

        with canvas(device) as draw:
            for i in range(len(actions)):
                draw.text((12, (i * 14)), actions[i],
                          font=widgets.get_font("small"), fill="white")

            wifi_status = widgets.get_widget("wifi")
            draw.text((80, (3 * 14)), wifi_status['itm'],
                      font=wifi_status['font'], fill="white")
            if not wifi.status():
                draw.rectangle([(80, (3 * 14)), (100, (3 * 14) + 8)],
                             fill="black")
            arrow = widgets.get_widget("gt")
            draw.text((0, (mn_itm * 13)), arrow['itm'],
                      font=arrow['font'], fill="white")

        

if __name__ == "__main__":
    from luma.core.interface.serial import i2c, spi
    from luma.oled.device import sh1106
    
    try:
        serial = spi(device=0, port=0, bus_speed_hz = 8000000, transfer_size = 4096, gpio_DC = DC_PIN, gpio_RST = RST_PIN)
        device = sh1106(serial, rotate=2)
        main(device)
    except KeyboardInterrupt:
        GPIO.cleanup()
        pass

