#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
widgets.py

Icons and fonts.
"""

from __future__ import unicode_literals
from PIL import ImageFont
import os

## icons that are outlines (v. few)
widgets = { "no_alarm": "\uf1f6", "alarm": "\uf0f3", "clock": "\uf017"
        }

## icons that are solids
solids = { "globe": "\uf0ac", "g_africa": "\uf57c",
           "g_asia": "\uf57e", "g_america": "\uf57d",
           "warning": "\uf071", "cog": "\uf013", "paw": "\uf1b0",
           "sync": "\uf021", "lock": "\uf023", "gt": "\uf105",
           "wifi": "\uf1eb", "not": "\uf00d",
           "up_a": "\uf106", "dn_a": "\uf107"
       }

## Setup fonts
def make_font(name, size):
    font_path = os.path.abspath(os.path.join(
        os.path.dirname(__file__), 'fonts', name))
    return ImageFont.truetype(font_path, size)

fonts = { 'big': make_font("code2000.ttf", 28),
          'med': make_font("FreePixel.ttf", 22),  ## 
          'small': make_font("ProggyTiny.ttf", 16),  ## 21 characters wide
          'wgt': make_font("fa5-regular.ttf", 14),
          'wgt2': make_font("fa5-solid.ttf", 14)
}

## load a font    
def get_font(name="big"):
    if name in fonts:
        return fonts[name]
    else:
        return fonts["small"]

## load an icon
def get_widget(name="paw"):
    if name in widgets:
        return { "itm": widgets[name], "font": get_font('wgt') }
    elif name in solids:
        return { "itm": solids[name], "font": get_font('wgt2') }
    else:
        return { "itm": solids["paw"], "font": get_font('wgt2') }

## commandline run test
if __name__ == "__main__":
    import sys

    print "{}: {}".format(sys.argv[1], get_widget(sys.argv[1]))
