#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
wifi.py

Wifi status and adjustment (on/off)
"""

from PIL import ImageFont
from luma.core.interface.serial import i2c, spi
from luma.core.render import canvas
from luma.core import lib
import widgets
import netifaces
import os

## is wifi connected...
def status():
    try:
        gws = netifaces.gateways()
        what = gws['default'][netifaces.AF_INET][1]
        iface = netifaces.ifaddresses(what)
        addr = iface[netifaces.AF_INET]
        ip = addr[0]['addr']
        if "wlan" in what:
            return True
        else:
            return False
        
    except:
        return False

## Turn off the network (should be the radio)
def turnoff():
    try:
        os.system("sudo ifconfig wlan0 down")
        return 1
    except:
        return 0

## Enable the wlan0 interface
def turnon():
    try:
        os.system("sudo ifconfig wlan0 up")
        return 1
    except:
        return 0
